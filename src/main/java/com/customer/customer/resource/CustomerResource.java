package com.customer.customer.resource;

import com.customer.customer.constants.APIConstant;
import com.customer.customer.errorhandling.ErrorMessage;
import com.customer.customer.model.Customer;
import com.customer.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class CustomerResource {
    private static final Logger logger = Logger.getLogger(CustomerResource.class.getName());
    private CustomerService customerService;
    @Autowired
    public CustomerResource(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     *
     * @param customer
     * @return
     */
    @PostMapping(
            path = APIConstant.BASE_PATH + APIConstant.POST_CUSTOMER,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> addCustomer(@RequestBody Customer customer) {
        Customer newCustomer = this.customerService.addCustomer(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body(newCustomer);
    }

    @DeleteMapping(
            path = APIConstant.BASE_PATH + APIConstant.GET_CUSTOMERS + APIConstant.CUSTOMER_ID,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> deleteCustomerById(@PathVariable("id") Long id) {
        boolean delete = this.customerService.removeCustomerById(id);
        if (delete) {
            logger.log(Level.INFO, "Customer with id "  + id + " deleted");
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(
                            new ErrorMessage(404, "Customer with id "
                                    + id + " not exist")
                    );
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping(
            path = APIConstant.BASE_PATH + APIConstant.GET_CUSTOMERS + APIConstant.CUSTOMER_ID,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Long id) {
        Optional<Customer> customer = this.customerService.getCustomerById(id);
        if (customer.isPresent()) {
            logger.log(Level.INFO, "Customer with id " + id + " found");
            return ResponseEntity.status(HttpStatus.OK).body(customer.get());
        } else {
            logger.log(Level.INFO, "Customer with id " + id + " not found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     *
     * @return
     */
    @GetMapping(
            path = APIConstant.BASE_PATH + APIConstant.GET_CUSTOMERS,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> getCustomers() {
        List<Customer> customers = this.customerService.getAllCustomer();
        logger.log(Level.INFO, "Number of total Customer " + customers.size());
        return ResponseEntity.status(HttpStatus.OK).body(customers);
    }
}
