package com.customer.customer.model;

import javax.validation.constraints.NotNull;

public class Customer {
    @NotNull
    private Long id;
    @NotNull
    private String firstname;
    @NotNull
    private String surname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
