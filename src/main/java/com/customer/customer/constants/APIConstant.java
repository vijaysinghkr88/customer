package com.customer.customer.constants;

public class APIConstant {
    public static final String BASE_PATH = "/api/v1";
    public static final String CUSTOMER_ID = "/customer/{id}";
    public static final String GET_CUSTOMERS = "/customers";
    public static final String POST_CUSTOMER = "/customers/customer";
}
