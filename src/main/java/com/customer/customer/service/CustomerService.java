package com.customer.customer.service;

import com.customer.customer.model.Customer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class CustomerService {
    private static final int ID_USER_1 = 1;
    private static final int ID_USER_2 = 2;
    private static final String FIRSTNAME_USER_1 = "test_one";
    private static final String SURNAME_USER_1 = "user";
    private static final String FIRSTNAME_USER_2 = "test_two";
    private static final String SURNAME_USER_2 = "user";
    public CustomerService() {}

    public Customer addCustomer(Customer customer) {
        if(customer.getId() == null) {
            long leftLimit = 1L;
            long rightLimit = 1000L;
            long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
            customer.setId(generatedLong);
        }
        return customer;
    }

    public boolean removeCustomerById(Long id) {
        if (id == null || (id >= 1000L)) {
            return false;
        } else {
            return true;
        }
    }

    public Optional<Customer> getCustomerById(final Long id) {
        return this.buildCustomer(id);
    }

    public List<Customer> getAllCustomer() {
        return this.buildCustomers();
    }

    private Optional<Customer> buildCustomer(final Long id) {
        Customer customer = this.buildCustomer(id, FIRSTNAME_USER_1, SURNAME_USER_1);
        Optional<Customer> foundCustomer = Optional.empty();
        foundCustomer = Optional.of(customer);
        return  foundCustomer;
    }

    private List<Customer> buildCustomers() {
        List<Customer> customers = new ArrayList<>();
        customers.add(this.buildCustomer(new Long(ID_USER_1), FIRSTNAME_USER_1, SURNAME_USER_1));
        customers.add(this.buildCustomer(new Long(ID_USER_2), FIRSTNAME_USER_2, SURNAME_USER_2));
        return customers;
    }

    private Customer buildCustomer(final Long id, final String firstName, final String lastName) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setFirstname(firstName);
        customer.setSurname(lastName);
        return customer;
    }
}
