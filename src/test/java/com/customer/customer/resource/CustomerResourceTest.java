package com.customer.customer.resource;

import com.customer.customer.model.Customer;
import com.customer.customer.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CustomerResourceTest {
    private CustomerService mockCustomerService;
    private CustomerResource customerResource;
    private static final int ID_USER_1 = 1;
    private static final int ID_USER_2 = 2;
    private static final String FIRSTNAME_USER_1 = "test_one";
    private static final String LASTNAME_USER_1 = "user";
    private static final String FIRSTNAME_USER_2 = "test_two";
    private static final String LASTNAME_USER_2 = "user";

    @Before
    public void setUp() {
       mockCustomerService = mock(CustomerService.class);
       customerResource = new CustomerResource(mockCustomerService);
    }

    @Test
    public void addCustomer() {
        Long id = new Long(ID_USER_1);
        when(mockCustomerService.addCustomer(any(Customer.class)))
                .thenReturn(buildCustomer(id, FIRSTNAME_USER_1, LASTNAME_USER_1));
        ResponseEntity<?> resposeEntity = customerResource
                .addCustomer(buildCustomer(id, FIRSTNAME_USER_1, LASTNAME_USER_1));
        Customer customer = (Customer) resposeEntity.getBody();
        assertEquals(201, resposeEntity.getStatusCode().value());
        assertEquals(id, customer.getId());
    }

    @Test
    public void deleteCustomerById() {
        Long id = new Long(ID_USER_1);
        when(mockCustomerService.removeCustomerById(id))
                .thenReturn(true);
        ResponseEntity<?> resposeEntity = customerResource
                .deleteCustomerById(id);
        assertEquals(200, resposeEntity.getStatusCode().value());
    }

    @Test
    public void getCustomerById() {
        Long id = new Long(ID_USER_1);
        when(mockCustomerService.getCustomerById(any(Long.class)))
                .thenReturn(Optional.of(buildCustomer(id, FIRSTNAME_USER_1, LASTNAME_USER_1)));
        ResponseEntity<?> resposeEntity = customerResource.getCustomerById(id);
        Customer customer = (Customer)resposeEntity.getBody();
        assertEquals(id, customer.getId());
        assertEquals(200, resposeEntity.getStatusCode().value());

    }

    @Test
    public void getCustomers() {
        when(mockCustomerService.getAllCustomer()).thenReturn(buildCustomers());
        ResponseEntity<?> responseEntity = customerResource.getCustomers();
        List<Customer> customerList = (List<Customer>)responseEntity.getBody();
        assertEquals(2, customerList.size());
        assertEquals(200, responseEntity.getStatusCode().value());
    }

    private List<Customer> buildCustomers() {
        List<Customer> customers = new ArrayList<>();
        customers.add(this.buildCustomer(new Long(ID_USER_1), FIRSTNAME_USER_1, LASTNAME_USER_1));
        customers.add(this.buildCustomer(new Long(ID_USER_2), FIRSTNAME_USER_2, LASTNAME_USER_2));
        return customers;
    }

    private Customer buildCustomer(final Long id, final String firstName, final String lastName) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setFirstname(firstName);
        customer.setSurname(lastName);
        return customer;
    }

}